package main

import (
	"com.gps/gk_testing/consul"
	dyEP "com.gps/gk_testing/helloconsul/pkg/endpoints"
	dyHttpHandler "com.gps/gk_testing/helloconsul/pkg/http"
	dySrv "com.gps/gk_testing/helloconsul/pkg/service"
	"fmt"
	logkit "github.com/go-kit/kit/log"
	uuid "github.com/satori/go.uuid"
	"log"
	"net/http"
	"os"
	"os/signal"
	"strconv"
	"syscall"
)

var (
	logger = log.Default()
	kitlog logkit.Logger
)

func main() {
	errChan := make(chan error)

	serviceHost := "192.168.168.16"
	servicePort := 9999
	serviceName := "SayHello"


	consulHost := "192.168.10.58"
	consulPort := 8500

	discoverClient, _ := consul.NewKitDiscoverHttpClient(consulHost, consulPort)
	instanceId := serviceName + "-" + uuid.NewV4().String()
	//
	go func() {
		coSrv := dySrv.New(discoverClient)
		coEp := dyEP.New(coSrv)
		coHttphanlder := dyHttpHandler.NewHTTPHandler(coEp)

		log.Println("Http Server start at port: " + strconv.Itoa(servicePort))
		if !discoverClient.Register(serviceName, instanceId, "/health_check",
			serviceHost, servicePort, nil, logger) {
			log.Printf("string-service for service %s failed.\n", serviceName)
			os.Exit(-1)
		}
		errChan <- http.ListenAndServe(":"+strconv.Itoa(servicePort), coHttphanlder)
	}()

	go func() {
		// 监控系统信号，等待 ctrl + c 系统信号通知服务关闭
		c := make(chan os.Signal, 1)
		signal.Notify(c, syscall.SIGINT, syscall.SIGTERM)
		errChan <- fmt.Errorf("%s", <-c)
	}()

	log.Println(<-errChan)
	//服务退出取消注册
	discoverClient.DeRegister(instanceId, logger)
}
