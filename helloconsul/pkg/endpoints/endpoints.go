package endpoints

import (
	"context"

	"com.gps/gk_testing/helloconsul/pkg/service"
	"github.com/go-kit/kit/endpoint"
)

// Endpoints collects all of the endpoints that compose the helloconsul service. It's
// meant to be used as a helper struct, to collect all of the endpoints into a
// single parameter.
type Endpoints struct {
	HealthCheckEndpoint endpoint.Endpoint
	SayHelloEndpoint    endpoint.Endpoint
}

// HealthCheckRequest collects the request parameters for the HealthCheck method.
type HealthCheckRequest struct{}

// HealthCheckResponse collects the response values for the HealthCheck method.
type HealthCheckResponse struct {
	Status bool `json:"status"`
}

// SayHelloRequest collects the request parameters for the SayHello method.
type SayHelloRequest struct{}

// SayHelloResponse collects the response values for the SayHello method.
type SayHelloResponse struct {
	Message string `json:"message"`
}

// New return a new instance of the endpoint that wraps the provided service.
func New(svc service.DiscoveryService) (ep Endpoints) {
	ep.HealthCheckEndpoint = MakeHealthCheckEndpoint(svc)
	ep.SayHelloEndpoint = MakeSayHelloEndpoint(svc)
	return ep
}

// MakeHealthCheckEndpoint returns an endpoint that invokes HealthCheck on the service.
// Primarily useful in a server.
func MakeHealthCheckEndpoint(svc service.DiscoveryService) (ep endpoint.Endpoint) {
	return func(ctx context.Context, _ interface{}) (interface{}, error) {
		status := svc.HealthCheck(ctx)
		return HealthCheckResponse{Status: status}, nil
	}
}

// MakeSayHelloEndpoint returns an endpoint that invokes SayHello on the service.
// Primarily useful in a server.
func MakeSayHelloEndpoint(svc service.DiscoveryService) (ep endpoint.Endpoint) {
	return func(ctx context.Context, _ interface{}) (interface{}, error) {
		message := svc.SayHello(ctx)
		return SayHelloResponse{Message: message}, nil
	}
}
