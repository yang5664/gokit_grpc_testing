package http

import (
	"com.gps/gk_testing/helloconsul/pkg/endpoints"
	"context"
	"encoding/json"
	httptransport "github.com/go-kit/kit/transport/http"
	"net/http"
)

// NewHTTPHandler returns a handler that makes a set of endpoints available on
// predefined paths.
func NewHTTPHandler(endpoints endpoints.Endpoints) http.Handler {
	m := http.NewServeMux()

	//options := []kithttp.ServerOption{
	//	kithttp.ServerErrorHandler(transport.NewLogErrorHandler(logger)),
	//	kithttp.ServerErrorEncoder(encodeError),
	//}

	m.Handle("/health_check", httptransport.NewServer(
		endpoints.HealthCheckEndpoint,
		DecodeHealthCheckRequest,
		EncodeHealthCheckResponse,
		//options...
	))
	m.Handle("/say_hello", httptransport.NewServer(
		endpoints.SayHelloEndpoint,
		DecodeSayHelloRequest,
		EncodeSayHelloResponse,
		//options...
	))
	return m
}

// DecodeHealthCheckRequest is a transport/http.DecodeRequestFunc that decodes a
// JSON-encoded request from the HTTP request body. Primarily useful in a server.
func DecodeHealthCheckRequest(_ context.Context, r *http.Request) (interface{}, error) {
	//req := endpoints.HealthCheckRequest{}
	//err := json.NewDecoder(r.Body).Decode(&req)
	return endpoints.HealthCheckRequest{}, nil
}

// EncodeHealthCheckResponse is a transport/http.EncodeResponseFunc that encodes
// the response as JSON to the response writer. Primarily useful in a server.
func EncodeHealthCheckResponse(_ context.Context, w http.ResponseWriter, res interface{}) (err error) {
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	err = json.NewEncoder(w).Encode(res)
	return err
}

// DecodeSayHelloRequest is a transport/http.DecodeRequestFunc that decodes a
// JSON-encoded request from the HTTP request body. Primarily useful in a server.
func DecodeSayHelloRequest(_ context.Context, r *http.Request) (interface{}, error) {
	req := endpoints.SayHelloRequest{}
	err := json.NewDecoder(r.Body).Decode(&req)
	return req, err
}

// EncodeSayHelloResponse is a transport/http.EncodeResponseFunc that encodes
// the response as JSON to the response writer. Primarily useful in a server.
func EncodeSayHelloResponse(_ context.Context, w http.ResponseWriter, res interface{}) (err error) {
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	err = json.NewEncoder(w).Encode(res)
	return err
}

func encodeError(ctx context.Context, err error, w http.ResponseWriter) {
	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	switch err {
	default:
		w.WriteHeader(http.StatusInternalServerError)
	}
	json.NewEncoder(w).Encode(map[string]interface{}{
		"error": err.Error(),
	})
}