package service

import (
	"com.gps/gk_testing/consul"
	"context"
)

// DiscoveryService implements yor service methods.
// e.x: Foo(ctx context.Context,s string)(rs string, err error)
type DiscoveryService interface {
	HealthCheck(ctx context.Context) (status bool)
	SayHello(ctx context.Context) (message string)
}
type stubDiscoveryService struct{
	discoveryClient consul.DiscoverHttpClient
}

// New return a new instance of the service.
// If you want to add service middleware this is the place to put them.
func New(discoveryClient consul.DiscoverHttpClient) (s DiscoveryService) {
	s = &stubDiscoveryService{
		discoveryClient: discoveryClient,
	}
	return s
}

// Implement the business logic of HealthCheck
func (di *stubDiscoveryService) HealthCheck(ctx context.Context) (status bool) {
	return true
}

// Implement the business logic of SayHello
func (di *stubDiscoveryService) SayHello(ctx context.Context) (message string) {
	return "Hello World!"
}
