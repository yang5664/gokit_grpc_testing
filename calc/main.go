package main

import (
	"com.gps/gk_testing/calc/pkg/endpoints"
	grpc2 "com.gps/gk_testing/calc/pkg/grpc"
	pb "com.gps/gk_testing/calc/pkg/grpc/pb"
	"com.gps/gk_testing/calc/pkg/service"
	"com.gps/gk_testing/consul"
	"fmt"
	uuid "github.com/satori/go.uuid"
	"google.golang.org/grpc"
	"google.golang.org/grpc/health/grpc_health_v1"
	"log"
	"net"
	"os"
	"os/signal"
	"strconv"
	"syscall"
)

var logger = log.Default()

func main() {
	errChan := make(chan error)

	calcSrv := service.New()
	calcEp := endpoints.New(calcSrv)
	calcHandler := grpc2.NewGRPCHandler(calcEp)

	lis, _ := net.Listen("tcp", "127.0.0.1:9999")

	serviceHost := lis.Addr().(*net.TCPAddr).IP.String()
	servicePort := lis.Addr().(*net.TCPAddr).Port
	serviceName := "CalcService"

	consulHost := "127.0.0.1"
	consulPort := 8500

	discoverClient, _ := consul.NewKitDiscoverGrpcClient(consulHost, consulPort)
	instanceId := serviceName + "-" + uuid.NewV4().String()

	go func() {
		baseServer := grpc.NewServer()
		pb.RegisterCalcServer(baseServer, calcHandler)
		grpc_health_v1.RegisterHealthServer(baseServer, calcSrv)

		log.Println("Http Server start at port: " + strconv.Itoa(servicePort))
		if !discoverClient.Register(serviceName, instanceId,
			serviceHost, servicePort, nil, logger) {
			log.Printf("string-service for service %s failed.\n", serviceName)
			os.Exit(-1)
		}
		errChan <- baseServer.Serve(lis)
	}()

	go func() {
		// 监控系统信号，等待 ctrl + c 系统信号通知服务关闭
		c := make(chan os.Signal, 1)
		signal.Notify(c, syscall.SIGINT, syscall.SIGTERM)
		errChan <- fmt.Errorf("%s", <-c)
	}()

	log.Println(<-errChan)
	//服务退出取消注册
	discoverClient.DeRegister(instanceId, logger)
}
