package service

import (
	"context"
	"errors"
	"google.golang.org/grpc/health/grpc_health_v1"
)

// CalcService implements yor service methods.
// e.x: Foo(ctx context.Context,s string)(rs string, err error)
type CalcService interface {
	Add(ctx context.Context, numA, numB float64) (rs float64, err error)
	Check(ctx context.Context, req *grpc_health_v1.HealthCheckRequest) (*grpc_health_v1.HealthCheckResponse, error)
	Watch(req *grpc_health_v1.HealthCheckRequest, w grpc_health_v1.Health_WatchServer) error
}

type stubCalcService struct{}

// New return a new instance of the service.
// If you want to add service middleware this is the place to put them.
func New() (s CalcService) {
	s = &stubCalcService{}
	return s
}

// Implement the business logic of Add
func (ca *stubCalcService) Add(ctx context.Context, numA float64, numB float64) (rs float64, err error) {
	rs = numA + numB
	return rs, errors.New("")
}

func (ca *stubCalcService) Check(ctx context.Context, req *grpc_health_v1.HealthCheckRequest) (*grpc_health_v1.HealthCheckResponse, error) {
	return &grpc_health_v1.HealthCheckResponse{
		Status: grpc_health_v1.HealthCheckResponse_SERVING,
	}, nil
}

func (ca *stubCalcService) Watch(req *grpc_health_v1.HealthCheckRequest, w grpc_health_v1.Health_WatchServer) error {
	return nil
}