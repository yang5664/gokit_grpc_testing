package http

import (
	"context"
	"encoding/json"
	"net/http"

	"com.gps/gk_testing/calc/pkg/endpoints"
	httptransport "github.com/go-kit/kit/transport/http"
)

// NewHTTPHandler returns a handler that makes a set of endpoints available on
// predefined paths.
func NewHTTPHandler(endpoints endpoints.Endpoints) http.Handler {
	m := http.NewServeMux()
	m.Handle("/add", httptransport.NewServer(
		endpoints.AddEndpoint,
		DecodeAddRequest,
		EncodeAddResponse,
	))
	return m
}

// DecodeAddRequest is a transport/http.DecodeRequestFunc that decodes a
// JSON-encoded request from the HTTP request body. Primarily useful in a server.
func DecodeAddRequest(_ context.Context, r *http.Request) (interface{}, error) {
	req := endpoints.AddRequest{}
	err := json.NewDecoder(r.Body).Decode(&req)
	return req, err
}

// EncodeAddResponse is a transport/http.EncodeResponseFunc that encodes
// the response as JSON to the response writer. Primarily useful in a server.
func EncodeAddResponse(_ context.Context, w http.ResponseWriter, res interface{}) (err error) {
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	err = json.NewEncoder(w).Encode(res)
	return err
}
