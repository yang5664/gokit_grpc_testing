package endpoints

import (
	"context"

	"com.gps/gk_testing/calc/pkg/service"
	"github.com/go-kit/kit/endpoint"
)

// Endpoints collects all of the endpoints that compose the calc service. It's
// meant to be used as a helper struct, to collect all of the endpoints into a
// single parameter.
type Endpoints struct {
	AddEndpoint endpoint.Endpoint
}

// AddRequest collects the request parameters for the Add method.
type AddRequest struct {
	NumA float64 `json:"num_a"`
	NumB float64 `json:"num_b"`
}

// AddResponse collects the response values for the Add method.
type AddResponse struct {
	Rs  float64 `json:"rs"`
	Err string `json:"err"`
}

// New return a new instance of the endpoint that wraps the provided service.
func New(svc service.CalcService) (ep Endpoints) {
	ep.AddEndpoint = MakeAddEndpoint(svc)
	return ep
}

// MakeAddEndpoint returns an endpoint that invokes Add on the service.
// Primarily useful in a server.
func MakeAddEndpoint(svc service.CalcService) (ep endpoint.Endpoint) {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(AddRequest)
		rs, err := svc.Add(ctx, req.NumA, req.NumB)
		return AddResponse{Rs: rs, Err: err.Error()}, nil
	}
}
