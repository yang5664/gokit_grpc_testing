package grpc

import (
	"com.gps/gk_testing/calc/pkg/endpoints"
	pb "com.gps/gk_testing/calc/pkg/grpc/pb"
	"context"
	grpctransport "github.com/go-kit/kit/transport/grpc"
	oldcontext "golang.org/x/net/context"
)

type grpcServer struct {
	add grpctransport.Handler
}

// NewGRPCHandler makes a set of endpoints available as a gRPC server.
func NewGRPCHandler(endpoints endpoints.Endpoints) (req pb.CalcServer) {
	req = &grpcServer{
		add: grpctransport.NewServer(
			endpoints.AddEndpoint,
			DecodeGRPCAddRequest,
			EncodeGRPCAddResponse,
		),
	}
	return req
}

// DecodeGRPCAddRequest is a transport/grpc.DecodeRequestFunc that converts a
// gRPC request to a user-domain request. Primarily useful in a server.
// TODO: Do not forget to implement the decoder, you can find an example here :
// https://github.com/go-kit/kit/blob/master/examples/addsvc/transport_grpc.go#L62-L65
func DecodeGRPCAddRequest(_ context.Context, grpcReq interface{}) (req interface{}, err error) {
	//err = errors.New("'Add' Decoder is not implement")
	r := grpcReq.(*pb.AddRequest)
	return endpoints.AddRequest{
		NumA: r.NumA,
		NumB: r.NumB,
	}, err
}

// EncodeGRPCAddResponse is a transport/grpc.EncodeResponseFunc that converts a
// user-domain response to a gRPC reply. Primarily useful in a server.
// TODO: Do not forget to implement the encoder, you can find an example here :
// https://github.com/go-kit/kit/blob/master/examples/addsvc/transport_grpc.go#L62-L65
func EncodeGRPCAddResponse(_ context.Context, grpcReply interface{}) (res interface{}, err error) {
	//err = errors.New("'Add' Encoder is not implement")
	r := grpcReply.(endpoints.AddResponse)
	return &pb.AddReply{
		Rs: r.Rs,
		Err: r.Err,
	}, err
}

func (s *grpcServer) Add(ctx oldcontext.Context, req *pb.AddRequest) (rep *pb.AddReply, err error) {
	_, rp, err := s.add.ServeGRPC(ctx, req)
	if err != nil {
		return nil, err
	}
	rep = rp.(*pb.AddReply)
	return rep, err
}
