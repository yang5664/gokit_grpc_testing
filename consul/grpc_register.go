package consul

import (
	"fmt"
	"github.com/go-kit/kit/sd/consul"
	"github.com/hashicorp/consul/api"
	"github.com/hashicorp/consul/api/watch"
	"log"
	"strconv"
	"sync"
)

type DiscoverGrpcClient interface{
	Register(serviceName, instanceId,
		instanceHost string, instancePort int, meta map[string]string, logger *log.Logger) bool

	DeRegister(instanceId string, logger *log.Logger) bool

	DiscoveryServices(serviceName string, logger *log.Logger) []interface{}
}

type KitDiscoverGrpcClient struct {
	consulHost  string          //consul的地址
	consulPort  int             //consul的端口
	client      consul.Client   //kit封装consul的客户端
	config      *api.Config     //kit封装的consul配置
	mutex       sync.Mutex
	instanceMap sync.Map
}

func NewKitDiscoverGrpcClient(consulHost string, consulPort int) (DiscoverGrpcClient, error) {
	consulConfig := api.DefaultConfig()
	consulConfig.Address = consulHost + ":" + strconv.Itoa(consulPort)
	apiClient, err := api.NewClient(consulConfig)
	if err != nil {
		return nil, err
	}
	client := consul.NewClient(apiClient)
	return &KitDiscoverGrpcClient{
		consulHost: consulHost,
		consulPort: consulPort,
		client:     client,
		config:     consulConfig,
	}, nil

}

func (k KitDiscoverGrpcClient) Register(serviceName, instanceId, instanceHost string, instancePort int, meta map[string]string, logger *log.Logger) bool {
	serviceRegistration := &api.AgentServiceRegistration{
		ID:      instanceId,
		Name:    serviceName,
		Address: instanceHost,
		Port:    instancePort,
		Meta:    meta,
		Tags: 	 []string{"primary"},
		Check: &api.AgentServiceCheck{
			DeregisterCriticalServiceAfter: "30s",
			GRPC: fmt.Sprintf("%v:%v/%v", instanceHost, instancePort, serviceName),
			Interval:                       "15s",
			Method :						"GET",
		},
	}

	err := k.client.Register(serviceRegistration)
	if err != nil {
		logger.Println("Register Service Error!")
		return false
	}
	logger.Println("Register Service Success!")
	return true
}

func (k KitDiscoverGrpcClient) DeRegister(instanceId string, logger *log.Logger) bool {
	serviceRegistration := &api.AgentServiceRegistration{
		ID: instanceId,
	}

	err := k.client.Deregister(serviceRegistration)
	if err != nil {
		logger.Println("Deregister Service Error!")
		return false
	}

	logger.Println("Deregister Service Success!")
	return true
}

func (k KitDiscoverGrpcClient) DiscoveryServices(serviceName string, logger *log.Logger) []interface{} {
	//判断服务是否已缓存
	instanceList, ok := k.instanceMap.Load(serviceName)
	if ok {
		return instanceList.([]interface{})
	}

	k.mutex.Lock()
	defer k.mutex.Unlock()
	//加锁后在判断一次，服务是否已缓存
	instanceList, ok = k.instanceMap.Load(serviceName)
	if ok {
		return instanceList.([]interface{})
	}

	//响应服务变更通知，更新服务map
	go func() {
		params := make(map[string]interface{})
		params["type"] = "service"
		params["service"] = serviceName
		plan, _ := watch.Parse(params)
		plan.Handler = func(u uint64, i interface{}) {
			if i == nil {
				return
			}

			v, ok := i.([]*api.ServiceEntry)
			if !ok {
				return
			}

			if len(v) == 0 {
				k.instanceMap.Store(serviceName, []interface{}{})
			}

			var healthServices []interface{}
			for _, service := range v {
				if service.Checks.AggregatedStatus() == api.HealthPassing {
					healthServices = append(healthServices, service)
				}
			}
			k.instanceMap.Store(serviceName, healthServices)
		}
		defer plan.Stop()
		plan.Run(k.config.Address)
	}()

	//调用go-kit库向consul获取服务
	entries, _, err := k.client.Service(serviceName, "", false, nil)
	if err != nil {
		k.instanceMap.Store(serviceName, []interface{}{})
		logger.Println("Discover Service Error")
		return nil
	}

	instances := make([]interface{}, 0, len(entries))
	for _, instance := range entries {
		instances = append(instances, instance)
	}

	k.instanceMap.Store(serviceName, instances)
	return instances
}