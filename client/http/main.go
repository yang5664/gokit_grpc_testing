package main

import (
	clientEp "com.gps/gk_testing/client/http/pkg/endpoints"
	http2 "com.gps/gk_testing/client/http/pkg/http"
	"net/http"
)

func main() {
	ep, _ := clientEp.NewHttpEndpoints("127.0.0.1:8081")
	h := http2.NewHTTPHandler(ep)

	server := &http.Server{
		Addr: ":9999",
		Handler: h,
	}

	server.ListenAndServe()
}
