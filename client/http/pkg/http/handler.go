package http

import (
	ep "com.gps/gk_testing/client/http/pkg/endpoints"
	"context"
	"encoding/json"
	"fmt"
	"net/http"
)

// NewHTTPHandler returns a handler that makes a set of endpoints available on
// predefined paths.
func NewHTTPHandler(endpoints ep.Endpoints) http.Handler {
	m := http.NewServeMux()
	ctx := context.Background()
	m.HandleFunc("/add", func(w http.ResponseWriter, r *http.Request) {
		req := ep.AddRequest{}
		json.NewDecoder(r.Body).Decode(&req)
		resp, err := endpoints.AddEndpoint(ctx, req)
		if err != nil {
			fmt.Println(err.Error())
		}
		//resp := res.(ep.AddResponse)
		json.NewEncoder(w).Encode(resp)
	})
	return m
}