package main

import (
	cep "com.gps/gk_testing/client/grpc/pkg/endpoints"
	"context"
	"fmt"
	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/sd"
	"github.com/go-kit/kit/sd/consul"
	"github.com/hashicorp/consul/api"
	"os"
)

func main() {
	//conn, err := grpc.Dial("127.0.0.1:9999", grpc.WithInsecure())
	//if err != nil {
	//	log.Fatalln("gRPC dial:", err)
	//}
	//defer conn.Close()
	// 直接使用PB提供的方法
	//client := pb.NewCalcClient(conn)
	//res, err := client.Add(context.Background(), &pb.AddRequest{
	//	NumA: 8,
	//	NumB: 16,
	//})
	//if err != nil {
	//	log.Fatal(err)
	//}
	//fmt.Println(res)
	//
	// 使用Endpoints集合
	//conn, err := grpc.Dial("127.0.0.1:9999", grpc.WithInsecure())
	//if err != nil {
	//	log.Fatal(err)
	//}
	//eps, _ := endpoints.NewGrpcEndpoints(conn)
	//resp, err := eps.AddEndpoint(context.Background(), endpoints.AddRequest{
	//	NumA: 10,
	//	NumB: 2,
	//})
	//_resp := resp.(endpoints.AddResponse)
	//fmt.Println(_resp.Rs)

	// Consul
	logger := log.NewLogfmtLogger(os.Stdout)
	config := api.DefaultConfig()
	apiClient, err := api.NewClient(config)
	if err != nil {
		logger.Log("Consul dial:", err)
	}
	client := consul.NewClient(apiClient)
	tags := []string{"primary"}

	instancer := consul.NewInstancer(client, logger, "CalcService", tags, true)

	addEndpointer := sd.NewEndpointer(instancer, cep.NewFactory, logger)

	addEndpoints, _ := addEndpointer.Endpoints()

	ep := addEndpoints[0]
	resp, _ := ep(context.Background(), cep.AddRequest{
		NumA: 11,
		NumB: 32,
	})
	fmt.Println(resp)

}
