package endpoints

import (
	pb "com.gps/gk_testing/client/grpc/pkg/grpc/pb"
	"context"
	"github.com/go-kit/kit/endpoint"
	grpctransport "github.com/go-kit/kit/transport/grpc"
	"google.golang.org/grpc"
	"io"
)

// AddRequest collects the request parameters for the Add method.
type AddRequest struct {
	NumA float64 `json:"num_a"`
	NumB float64 `json:"num_b"`
}

// AddResponse collects the response values for the Add method.
type AddResponse struct {
	Rs  float64 `json:"rs"`
	Err string `json:"err"`
}

// Endpoints collects all of the endpoints that compose the calc service. It's
// meant to be used as a helper struct, to collect all of the endpoints into a
// single parameter.
type Endpoints struct {
	AddEndpoint endpoint.Endpoint
}

func NewFactory(instance string) (endpoint.Endpoint, io.Closer, error) {
	conn, err := grpc.Dial(instance, grpc.WithInsecure())
	if err != nil {
		return nil, nil, err
	}
	ep, _ := NewGrpcEndpoints(conn)
	return ep.AddEndpoint, conn, nil
}

func NewGrpcEndpoints(conn *grpc.ClientConn) (ep Endpoints, err error) {
	ep.AddEndpoint = grpctransport.NewClient(conn, "pb.Calc", "Add", EncodeAddRequestFunc, DecodeAddResponseFunc, pb.AddReply{}).Endpoint()
	return ep, nil
}

func EncodeAddRequestFunc(ctx context.Context, req interface{}) (request interface{}, err error) {
	_req := req.(AddRequest)
	return &pb.AddRequest{
		NumA: _req.NumA,
		NumB: _req.NumB,
	}, nil
}

func DecodeAddResponseFunc(ctx context.Context, resp interface{}) (response interface{}, err error) {
	_resp := resp.(*pb.AddReply)
	return AddResponse{
		Rs: _resp.Rs,
		Err: _resp.Err,
	}, nil
}