package main

import (
	calcEP "com.gps/gk_testing/calc/pkg/endpoints"
	calcGrpcHandler "com.gps/gk_testing/calc/pkg/grpc"
	pb "com.gps/gk_testing/calc/pkg/grpc/pb"
	calcHttpHandler "com.gps/gk_testing/calc/pkg/http"
	calcSrv "com.gps/gk_testing/calc/pkg/service"
	"google.golang.org/grpc"
	"log"
	"net"
	"net/http"
)

func main() {
	errChan := make(chan error)

	srv := calcSrv.New()
	ep := calcEP.New(srv)
	httphandler := calcHttpHandler.NewHTTPHandler(ep)
	grpchandler := calcGrpcHandler.NewGRPCHandler(ep)
	//
	go func() {
		server := &http.Server{
			Addr: ":8081",
			Handler: httphandler,
		}
		errChan <- server.ListenAndServe()
	}()

	go func() {
		baseServer := grpc.NewServer()
		pb.RegisterCalcServer(baseServer, grpchandler)
		tls, _ := net.Listen("tcp", ":5001")
		errChan <- baseServer.Serve(tls)
	}()

	log.Println(<-errChan)
}
